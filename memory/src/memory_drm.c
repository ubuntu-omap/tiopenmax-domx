/* DRM support for memory allocation */

#include "omx_rpc_utils.h"
#include "memory.h"

#include <omap/omap_drm.h>
#include <libdrm/omap_drmif.h>

static int open_refcount;

static struct {
    int fd;
    struct omap_device *dev;
} drm_data;

static int drm_open() {
    if (open_refcount < 0) {
        return -1;
    }
    if (open_refcount++ > 0) {
        return 0;
    }
    open_refcount = -1;
    drm_data.fd = drmOpen("omapdrm", NULL);
    if (drm_data.fd < 0) {
        DOMX_ERROR("%m opening DRM");
        return -1;
    }
    drm_data.dev = omap_device_new(drm_data.fd);
    if (drm_data.dev == 0) {
        DOMX_ERROR("%m creating DRM device");
        return -1;
    }
    open_refcount = 1;
    return 0;
}

static int drm_close() {
    if (open_refcount < 0) {
        return -1;
    }
    if (--open_refcount > 0) {
        return 0;
    }
    omap_device_del(drm_data.dev);
    return close(drm_data.fd);
}

static int drm_alloc(memory_buffer_t* buffer, size_t size) {
    struct omap_bo *bo = omap_bo_new(drm_data.dev, size, OMAP_BO_SCANOUT | OMAP_BO_WC);
    if (bo == NULL) {
        DOMX_ERROR("%m allocating DRM memory");
        goto err_bo;
    }
    int fd = omap_bo_dmabuf(bo);
    if (fd < 0) {
        DOMX_ERROR("%m getting DMA buf file descriptor");
        goto err_fd;
    }
    void* buf = omap_bo_map(bo);
    if (buf == NULL) {
        DOMX_ERROR("%m mapping DMA buf");
        goto err_map;
    }
    buffer->buf = buf;
    buffer->fd = fd;
    buffer->private = bo;
    return 0;
err_map:
    close(fd);
err_fd:
    omap_bo_del(bo);
err_bo:
    return -1;
}

static int drm_free(memory_buffer_t* buffer) {
    close(buffer->fd);
    struct omap_bo *bo = buffer->private;
    omap_bo_del(bo);
    return 0;
}

int memory_alloc(memory_buffer_t* buffer, size_t size) {
    if (drm_open() < 0) {
        return -1;
    }
    if (drm_alloc(buffer, size) < 0) {
        return -1;
    }
    return 0;
}

void memory_free(memory_buffer_t* buffer) {
    buffer->buf = NULL;
    buffer->fd = -1;
    buffer->private = NULL;
}
