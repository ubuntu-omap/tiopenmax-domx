/* LibC support for memory allocation */

#include "memory.h"

#include <stdlib.h>

int memory_alloc(memory_buffer_t* buffer, size_t size) {
    void* buf = malloc(size);
    if (buf == NULL) {
        return -1;
    }
    buffer->buf = buf;
    buffer->fd = -1;
    buffer->private = NULL;
    return 0;
}

void memory_free(memory_buffer_t* buffer) {
    free(buffer->buf);
    buffer->buf = NULL;
}
