/*
 * Copyright (c) 2012 Texas Instruments Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/** memory.h
 * Memory allocation/free functions, actual implementation to be decided at
 * compile time.
 */

#ifndef _DOMX_MEMORY_H
#define _DOMX_MEMORY_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct {
  void* buf;        /**< pointer to the buffer */
  int   fd;         /**< buffer file descriptor, if >= 0 */
  void* private;    /**< internal data pointer, do not touch */
} memory_buffer_t;

/** This function tries to allocate a buffer. If the underlying system needs
 * opening, it will be open inside it, and kept open forever, so this is a
 * singleton.

    @param buffer
        buffer structure to be filled in with the right data.
    @param size
        requested buffer size
    @return 0 on success, negative otherwise
    @ingroup buf
*/
int memory_alloc(memory_buffer_t* buffer, size_t size);

/** This function frees a buffer.

    @param buffer
        structure of the buffer to be freed.
    @ingroup buf
*/
void memory_free(memory_buffer_t* buffer);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _DOMX_MEMORY_H */
