/* ====================================================================
 *   Copyright (C) 2010 Texas Instruments Incorporated
 *
 *   All rights reserved. Property of Texas Instruments Incorporated.
 *   Restricted rights to use, duplicate or disclose this code are
 *   granted through contract.
 *
 *   The program may not be used without the written permission
 *   of Texas Instruments Incorporated or against the terms and conditions
 *   stipulated in the agreement under which this program has been
 *   supplied.
 * ==================================================================== */

/*!
 *****************************************************************************
 * \file
 *    domx/omx_core/inc/msp_utils.h
 *
 * \brief
 *  TODO: Add description
 *
 * \version 1.0
 *
 *****************************************************************************
 */
#ifndef _OMX_MOD_CORRECTIONS_H
#define _OMX_MOD_CORRECTIONS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* User code goes here */
/* ------compilation control switches ----------------------------------------*/
/* If MSP_TRACE_ENABLE is defined, then all MSP traces are enabled. Else, it is
 * as per TIMM_OSAL control*/
//#define MSP_TRACE_ENABLE
/****************************************************************
 * INCLUDE FILES
 ****************************************************************/
/* ----- system and platform files ----------------------------*/
#include <OMX_Types.h>

/*-------program files ----------------------------------------*/

/****************************************************************
 * EXTERNAL REFERENCES NOTE : only use if not found in header file
 ****************************************************************/
/*--------data declarations -----------------------------------*/
/*--------function prototypes ---------------------------------*/
/****************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ****************************************************************/
/*--------macros ----------------------------------------------*/
/** public data **/

/* 16 Extended to allow precise H3A for preview */
#define OMX_MAX_WINHC                       (36)
/* 32 Extended to allow precise H3A for preview */
#define OMX_MAX_WINVC                       (128)
/* One paxel is 32 Bytes + on every 8 paxel 8*2 Bytes for number of unsaturated pixels in previouse 8 paxels */
#define OMX_AWWB_H3A_PAXEL_SIZE_BYTES       (32 + 2)
/* Max buffer size for AEWB H3A */
#define OMX_AEWB_H3A_MAX_BUFF_SIZE          (OMX_MAX_WINHC * OMX_MAX_WINVC * OMX_AWWB_H3A_PAXEL_SIZE_BYTES)
/*AF SCM Range Constants */
#define OMX_AF_PAXEL_VERTICAL_COUNT_MAX     (127)
/*AF SCM Range Constants */
#define OMX_AF_PAXEL_HORIZONTAL_COUNT_MAX   (35)
/* Max buffer size for AF H3A */
#define OMX_AF_H3A_MAX_BUFF_SIZE            (OMX_AF_PAXEL_VERTICAL_COUNT_MAX * OMX_AF_PAXEL_HORIZONTAL_COUNT_MAX * 48)
/* Max sensor with */
#define OMX_ISP_IN_WIDTH                    (4032)
/* Max sensor height */
#define OMX_ISP_IN_HEIGHT                   (3024)
/* Max buffer size for IPIPE BOXCAR */
#define OMX_IPIPE_BOXCAR_MAX_BUFF_SIZE      (((OMX_ISP_IN_WIDTH / 8) * (OMX_ISP_IN_HEIGHT / 8)) * 8)


//!< Noise filter number of THR coefficients
#define OMX_ISS_NF_THR_COUNT        (8)
//!< Noise filter number of STR coefficients
#define OMX_ISS_NF_STR_COUNT        (8)
//!< Noise filter number of SPR coefficients
#define OMX_ISS_NF_SPR_COUNT        (8)
//!< Numbers of values in Gamma table
#define OMX_ISS_PREV_GAMMA_TABLE    (1024)
//!< Number of offset values on table
#define OMX_ISS_PREV_RGB2RGB_OFFSET (3)
//!< Numbers of values in rows and colomns
#define OMX_ISS_PREV_RGB2RGB_MATRIX (3)
//!< GBCE enhancement table size
#define OMX_ISS_GBCE_TABLE_SIZE     (1024)
//!< Edge enhancement table size
#define OMX_ISS_EE_TABLE_SIZE       (1024)
//!< Edge enhancement number of coefficients
#define OMX_ISS_COEFF               (9)
//!< Histogram dims count
#define OMX_ISS_HIST_DIMS_COUNT     (4)
//!< Histogram gain table size
#define OMX_ISS_HIST_GAIN_TBL       (4)
//!< 3d lut table size
#define OMX_ISS_3D_LUT_SIZE         (729)
//!< Number of offset values on table
#define OMX_ISS_PREV_RGB2YUV_OFFSET (3)
//!< Numbers of values in rows and colomns
#define OMX_ISS_PREV_RGB2YUV_MATRIX (3)

/*--------data declarations -----------------------------------*/
typedef enum {
    OMX_IPIPE_BAYER_PATTERN_RGGB,
    OMX_IPIPE_BAYER_PATTERN_GRBG,
    OMX_IPIPE_BAYER_PATTERN_GBRG,
    OMX_IPIPE_BAYER_PATTERN_BGGR,
    OMX_IPIPE_BAYER_PATTERN_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_BAYER_PATTERNTYPE;

typedef enum {
    OMX_IPIPE_BAYER_MSB_BIT15,
    OMX_IPIPE_BAYER_MSB_BIT14,
    OMX_IPIPE_BAYER_MSB_BIT13,
    OMX_IPIPE_BAYER_MSB_BIT12,
    OMX_IPIPE_BAYER_MSB_BIT11,
    OMX_IPIPE_BAYER_MSB_BIT10,
    OMX_IPIPE_BAYER_MSB_BIT9,
    OMX_IPIPE_BAYER_MSB_BIT8,
    OMX_IPIPE_BAYER_MSB_BIT7,
    OMX_IPIPE_BAYER_MSB_BIT_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_BAYER_MSB_POSTYPE;

// TODO Every type should start with 'OMX_TI_3ASKIP' and ends with 'TYPE'
typedef enum {
    OMX_ISIF_LSC_8_PIXEL       = 3,
    OMX_ISIF_LSC_16_PIXEL      = 4,
    OMX_ISIF_LSC_32_PIXEL      = 5,
    OMX_ISIF_LSC_64_PIXEL      = 6,
    OMX_ISIF_LSC_128_PIXEL     = 7,
    OMX_ISIF_LSC_128_PIXEL_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_LSC_GAIN_MODE_MNTYPE;

typedef enum {
    OMX_LSC_GAIN_0Q8        = 0,
    OMX_LSC_GAIN_0Q8_PLUS_1 = 1,
    OMX_LSC_GAIN_1Q7        = 2,
    OMX_LSC_GAIN_1Q7_PLUS_1 = 3,
    OMX_LSC_GAIN_2Q6        = 4,
    OMX_LSC_GAIN_2Q6_PLUS_1 = 5,
    OMX_LSC_GAIN_3Q5        = 6,
    OMX_LSC_GAIN_3Q5_PLUS_1 = 7,
    OMX_LSC_GAIN_MAX        = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_LSC_GAIN_FORMATTYPE;

typedef enum {
    OMX_ISIF_LSC_OFFSET_NO_SHIFT         = 0,
    OMX_ISIF_LSC_OFFSET_1_LEFT_SHIFT     = 1,
    OMX_ISIF_LSC_OFFSET_2_LEFT_SHIFT     = 2,
    OMX_ISIF_LSC_OFFSET_3_LEFT_SHIFT     = 3,
    OMX_ISIF_LSC_OFFSET_4_LEFT_SHIFT     = 4,
    OMX_ISIF_LSC_OFFSET_5_LEFT_SHIFT     = 5,
    OMX_ISIF_LSC_OFFSET_5_LEFT_SHIFT_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_LSC_OFFSET_SHIFTTYPE;

typedef enum {
    OMX_ISIF_LSC_OFFSET_OFF = 0,
    OMX_ISIF_LSC_OFFSET_ON  = 1,
    OMX_ISIF_LSC_OFFSET_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_LSC_OFFSET_ENABLETYPE;

typedef struct {
    //!< DLSCCFG ENABLE-- enable 2d LSC module
    OMX_U8 nEnable;
    //!< HVAL LSCHVAL--LSC HSIZE
    OMX_U16 nLscHSize;
    //!< HVAL LSCHVAL--LSC VSIZE
    OMX_U16 nLscVSize;
    //!< HOFST LSCHOFST DATAHOFST 0-16383-- H-direction data offset
    OMX_U16 nHDirDataOffset;
    //!< VOFST LSCVOFST DATAHOFST 0-16383-- V-direction data offset
    OMX_U16 nVDirDataOffset;
    //!< X DLSCINI   6:0-- H-position of the paxel
    OMX_U8 nHPosInPaxel;
    //!< Y DLSCINI   6:0-- V-position of the paxel
    OMX_U8 nVPosInPaxel;

    //!< GAIN_MODE_M DLSCCFG
    OMX_TI_3ASKIP_ISIF_LSC_GAIN_MODE_MNTYPE ePaxHeight;
    //!< GAIN_MODE_N DLSCCFG
    OMX_TI_3ASKIP_ISIF_LSC_GAIN_MODE_MNTYPE ePaxLength;
    //!< GAIN_FORMAT DLSCCFG
    OMX_TI_3ASKIP_ISIF_LSC_GAIN_FORMATTYPE eGainFormat;
    //!< offset scaling factor
    OMX_U8 nOffsetScalingFactor;
    //!< OFSTSFT DLSCOFST--offset shift value
    OMX_TI_3ASKIP_ISIF_LSC_OFFSET_SHIFTTYPE eOffsetShiftVal;
    //!< OFSTSFT DLSCOFST--offset enable value
    OMX_TI_3ASKIP_ISIF_LSC_OFFSET_ENABLETYPE eOffsetEnable;
    //!< gain table address--32 bit aligned
    OMX_U32 pGainTableAddress[256];
    //!< gain table length
    OMX_U16 nGainTableLength;
    //!< offset table address
    OMX_U32 pOffsetTableAddress[256];
    //!< offset table length
    OMX_U16 nOffsetTableLength;
} OMX_TI_3ASKIP_ISIF_2DLSC_CFGTYPE;

typedef enum {
    OMX_ISIF_HORIZONTAL_CLAMP_DISABLED             = 0,
    OMX_ISIF_HORIZONTAL_CLAMP_ENABLED              = 1,
    OMX_ISIF_PREVIOUS_HORIZONTAL_CLAMP_ENABLED     = 2,
    OMX_ISIF_CLAMP_MAX                             = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_HORIZONTAL_CLAMP_MODETYPE;

typedef enum {
    OMX_ISIF_ONE_COLOR_CLAMP    = 0,
    OMX_ISIF_FOUR_COLOR_CLAMP   = 1,
    OMX_ISIF_COLOR_CLAMP_MAX    = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_BLACK_CLAMP_MODETYPE;

typedef enum {
    OMX_ISIF_HBLACK_2PIXEL_TALL    = 0,
    OMX_ISIF_HBLACK_4PIXEL_TALL    = 1,
    OMX_ISIF_HBLACK_8PIXEL_TALL    = 2,
    OMX_ISIF_HBLACK_16PIXEL_TALL   = 3,
    OMX_ISIF_HBLACK_PIXEL_TALL_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_HBLACK_PIXEL_HEIGHTTYPE;

typedef enum {
    OMX_ISIF_HBLACK_32PIXEL_WIDE   = 0,
    OMX_ISIF_HBLACK_64PIXEL_WIDE   = 1,
    OMX_ISIF_HBLACK_128PIXEL_WIDE  = 2,
    OMX_ISIF_HBLACK_256PIXEL_WIDE  = 3,
    OMX_ISIF_HBLACK_PIXEL_WIDE_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_HBLACK_PIXEL_WIDTHTYPE;

typedef enum {
    OMX_ISIF_VBLACK_PIXEL_NOT_LIMITED = 0,
    OMX_ISIF_VBLACK_PIXEL_LIMITED     = 1,
    OMX_ISIF_VBLACK_PIXEL_LIMITED_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_HBLACK_PIXEL_VALUE_LIMTYPE;

typedef enum {
    OMX_ISIF_VBLACK_BASE_WINDOW_LEFT  = 0,
    OMX_ISIF_VBLACK_BASE_WINDOW_RIGHT = 1,
    OMX_ISIF_VBLACK_BASE_WINDOW_MAX   = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_HBLACK_BASE_WINDOWTYPE;

typedef struct {
    //!< CLHSV CLHWIN2 12-0--Vertical black clamp.
    //!< Window Start position (V).Range: 0 - 8191
    OMX_U16 nVPos;
    //!< CLHWN  CLHWIN0--[Horizontal Black clamp]
    //!< Vertical dimension of a Window (2^N).
    OMX_TI_3ASKIP_ISIF_HBLACK_PIXEL_HEIGHTTYPE eVSize;
    //!< CLHSH CLHWIN1 12-0--Horizontal black clamp.
    //!< Window Start position (H).Range: 0 - 8191
    OMX_U16 nHPos;
    //!< CLHWM  CLHWIN0--[Horizontal Black clamp]
    //!< Horizontal dimension of a Window (2^M).
    OMX_TI_3ASKIP_ISIF_HBLACK_PIXEL_WIDTHTYPE eHSize;
    //!< CLHLMT CLHWIN0--Horizontal Black clamp. Pixel value
    //!< limitation for the Horizontal clamp value calculation
    OMX_TI_3ASKIP_ISIF_HBLACK_PIXEL_VALUE_LIMTYPE ePixelValueLimit;
    //!< CLHWBS CLHWIN0--[Horizontal Black clamp] Base Window select
    OMX_TI_3ASKIP_ISIF_HBLACK_BASE_WINDOWTYPE eRightWindow;
    //!< CLHWC  CLHWIN0--[Horizontal Black clamp]
    //!< Window count per color. Window count = CLHWC+1. Range: 1 - 32
    OMX_U8 nWindowCountPerColor;
} OMX_TI_3ASKIP_ISIFHBLACKPARAMSTYPE;

typedef enum {
    OMX_ISIF_VBLACK_2PIXEL_WIDE  = 0,
    OMX_ISIF_VBLACK_4PIXEL_WIDE  = 1,
    OMX_ISIF_VBLACK_8PIXEL_WIDE  = 2,
    OMX_ISIF_VBLACK_16PIXEL_WIDE = 3,
    OMX_ISIF_VBLACK_32PIXEL_WIDE = 4,
    OMX_ISIF_VBLACK_64PIXEL_WIDE = 5,
    OMX_ISIF_VBLACK_PIXEL_WIDE   = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_VBLACK_PIXEL_WIDTHTYPE;

typedef enum {
    OMX_ISIF_VALUE_HORIZONTAL_DIRECTION = 0,
    OMX_ISIF_VALUE_CONFIG_REGISTER      = 1,
    OMX_ISIF_VALUE_NOUPDATE             = 2,
    OMX_ISIF_VALUE_MAX                  = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_VBLACK_RESET_MODETYPE;

typedef struct {
    //!< CLVSV CLVWIN2 12-0--Vertical black clamp.
    //!< Window Start position (V).Range: 0 - 8191
    OMX_U16 nVPos;
    //!< CLVOBV CLVWIN3 12-0 range 0-8192-- Vertical black clamp.
    //!< Optical black V valid (V).Range: 0 - 8191
    OMX_U16 nVSize;
    //!< CLVSH CLVWIN1 12-0--Vertical black clamp. Window Start
    //!< position (H).Range: 0 - 8191
    OMX_U16 nHPos;
    //!< CLVOBH  CLVWIN0-- Vertical Black clamp. Optical Black H valid (2^L).
    OMX_TI_3ASKIP_ISIF_VBLACK_PIXEL_WIDTHTYPE HSize;
    //!< CLVCOEF CLVWIN0-- Vertical Black clamp .Line average coefficient (k).
    OMX_U8 line_avg_coef;
    //!< CLVRVSL CLVWIN0-- Vertical Black clamp. Select the reset value
    //!< for the Clamp value of the Previous line
    OMX_TI_3ASKIP_ISIF_VBLACK_RESET_MODETYPE reset_mode;
    //!< CLVRV reset value U12--Vertical black clamp reset value
    //!< (U12) Range: 0 to 4095
    OMX_U16 reset_value;
} OMX_TI_3ASKIP_ISIF_VERTICAL_BLACK_PARAMSTYPE;

typedef struct {
    //!< CLEN  CLAMPCFG-- clamp module enablement
    OMX_U8 nEnable;
    //!< CLMD  CLAMPCFG-- horizontal clamp mode
    OMX_TI_3ASKIP_ISIF_HORIZONTAL_CLAMP_MODETYPE eHClampMode;
    //!< CLHMD CLAMPCFG-- black clamp mode
    OMX_TI_3ASKIP_ISIF_BLACK_CLAMP_MODETYPE eBlackClampMode;
    //!< CLDCOFST CLDC s13-- clamp dc-offset value
    OMX_U16 nDCOffsetClampVal;
    //!< CLSV 12-0 (range 0-8191)--black clamp v-start position
    OMX_U16 nBlackClampVStartPos;
    //!< CLHWIN0-- horizontal black clamp parameters
    OMX_TI_3ASKIP_ISIF_VERTICAL_BLACK_PARAMSTYPE tHorizontalBlack;
    //!< CLVWIN0-- vertical black clamp parameters
    OMX_TI_3ASKIP_ISIF_VERTICAL_BLACK_PARAMSTYPE tVerticalBlack;
} OMX_TI_3ASKIP_ISIF_CLAMP_CFGTYPE;

typedef struct {
    //!< FLSHEN FLSHCFG0-- flash enable
    OMX_U8 nEnable;
    //!< SFLSH FLSHCFG1--Start line to set the FLASH timing signal.
    OMX_U16 nFlashTimingStartLine;
    //!< VFLSH FLSHCFG--Valid width of the FLASH timing signal.
    //!< Valid width = Crystal-clock x 2 x (VFLSH + 1)
    OMX_U16 nFlashTimingWidth;
} OMX_TI_3ASKIP_ISIF_FLASH_CFGTYPE;

typedef struct {
    //!< gain offset feature-flag
    OMX_U8 gain_offset_featureflag;
    //!< CGR  CRGAIN-- gain R
    OMX_U16 gain_r;
    //!< CGGR CGRGAIN-- gain GR
    OMX_U16 gain_gr;
    //!< CGGB CGBGAIN-- gain GB
    OMX_U16 gain_gb;
    OMX_U16 gain_bg;
    //!< COFT COFSTA--offset
    OMX_U16 offset;
} OMX_TI_3ASKIP_ISIF_GAINOFFSET_CFGTYPE;

typedef enum {
    OMX_ISIF_VDLC_WHOLE_LINE                = 0,
    OMX_ISIF_VDLC_DISABLE_ABOVE_UPPER_PIXEL = 1,
    OMX_ISIF_VDLC_MAX                       = 0x7FFFFFFF

} OMX_TI_3ASKIP_ISIF_VDLC_PIXEL_DEPENDENCYTYPE;

typedef enum {
    OMX_ISIF_VLDC_FED_THRO_ONSATURATION                 = 0,
    OMX_ISIF_VLDC_HORIZONTAL_INTERPOLATION_ONSATURATION = 1,
    OMX_ISIF_VLDC_HORIZONTAL_INTERPOLATION              = 2,
    OMX_ISIF_VLDC_MAX                                   = 0x7FFFFFFF
} OMX_TI_3ASKIP_ISIF_VLDC_MODE_SELECTTYPE;

typedef struct {
    //!< DFCMEM0--Vertical Defect position
    OMX_U16 nVerticalDefectPosition;
    //!< DFCMEM1 12-0--horizontal defect position
    OMX_U16 nHorizontalDefectPosition;
    //!< DFCMEM2--Defect correction Memory 2
    OMX_U8 nSub1ValueVldc;
    //!< DFCMEM3--Defect correction Memory 3
    OMX_U8 nSub2LessThanVldc;
    //!< DFCMEM4--Defect correction Memory 4
    OMX_U8 nSub3GreaterThanVldc;
} OMX_TI_3ASKIP_ISIF_VLDCDEFECT_LINEPARAMSTYPE;

typedef struct {
    //!< VDFCEN  DFCCTL--enable VLDC module
    OMX_U8 nEnable;
    //!< VDFCUDA DFCCTL--pixel dependency
    OMX_TI_3ASKIP_ISIF_VDLC_PIXEL_DEPENDENCYTYPE eDisableVldcUpperPixels;
    //!< VDFLSFT DFCCTL-- VLDC shift values
    OMX_U8 nVldcShiftVal;
    //!< VDFCSL  DFCCTL--VLDC mode select
    OMX_TI_3ASKIP_ISIF_VLDC_MODE_SELECTTYPE eVldcModeSelect;
    //!< VDFSLV VDFSATLV OMX_U12 range 0 - 4095-- VLDC saturation level
    OMX_U16 nVldcSaturationLvl;
    //!< number of defect lines-maximum8
    OMX_U8 nDefectLines;
    //!< DFCMEM0 -8--  defect line paramaters
    OMX_TI_3ASKIP_ISIF_VLDCDEFECT_LINEPARAMSTYPE tVldcDefectLineParams;
} OMX_TI_3ASKIP_ISIF_VLDC_CFGTYPE;

typedef enum {
    OMX_NOISE_FILTER_1   = 1,
    OMX_NOISE_FILTER_2   = 2,
    OMX_NOISE_FILTER_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_NOISE_FILTERTYPE;

typedef enum {
    OMX_IPIPE_NF_SPR_SINGLE = 0,
    OMX_IPIPE_NF_SPR_LUT    = 1,
    OMX_IPIPE_NF_SPR_MAX    = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_NF_SELTYPE;

typedef enum {
    OMX_IPIPE_NF_LSC_GAIN_OFF  = 0,
    OMX_IPIPE_NF_LSC_GAIN_ON   = 1,
    OMX_IPIPE_NF_LSC_GAIN_MAX  = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_NF_LSC_GAINTYPE;

typedef enum {
    OMX_IPIPE_NF_SAMPLE_BOX     = 0,
    OMX_IPIPE_NF_SAMPLE_DIAMOND = 1,
    OMX_IPIPE_NF_SAMPLE_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_NF_SAMPLE_METHODTYPE;

typedef struct {
    //!< Enable-disable for Noise filter in ipipe
    OMX_U8 nEnable;
    //!< Noise filter number
    OMX_TI_3ASKIP_IPIPE_NOISE_FILTERTYPE eNFNum;
    //!< Selecting the spread in NF
    OMX_TI_3ASKIP_IPIPE_NF_SELTYPE eSel;
    //!< Controling the lsc gain applied in Noise Filter
    OMX_TI_3ASKIP_IPIPE_NF_LSC_GAINTYPE eLscGain;
    //!< Selecting the sampling method
    OMX_TI_3ASKIP_IPIPE_NF_SAMPLE_METHODTYPE eTyp;
    OMX_U8                                   nDownShiftVal;
    OMX_U8                                   nSpread;
    OMX_U16                                  pThr[OMX_ISS_NF_THR_COUNT];
    OMX_U8                                   pStr[OMX_ISS_NF_STR_COUNT];
    OMX_U8                                   pSpr[OMX_ISS_NF_SPR_COUNT];
    OMX_U16                                  nEdgeMin;
    OMX_U16                                  nEdgeMax;
} OMX_TI_3ASKIP_IPIPE_NOISE_FILTER_CFGTYPE;

typedef enum {
    OMX_IPIPE_GIC_LSC_GAIN_OFF = 0,
    OMX_IPIPE_GIC_LSC_GAIN_ON  = 1,
    OMX_IPIPE_GIC_LSC_GAIN_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_GIC_LSC_GAINTYPE;

typedef enum {
    OMX_IPIPE_GIC_GICTHR = 0,
    OMX_IPIPE_GIC_NF2THR = 1,
    OMX_IPIPE_GIC_MAX    = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_GIC_SELTYPE;

typedef enum {
    OMX_IPIPE_GIC_DIFF_INDEX = 0,
    OMX_IPIPE_GIC_HPD_INDEX  = 1,
    OMX_IPIPE_GIC_INDEX_MAX  = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_GIC_INDEXTYPE;

typedef struct {
    /* works only when data format is GR,GB */
    //!< Enable-disable for diffrerent components in ipipe
    OMX_U16 nEnable;
    //!< Selecting LSC gain in GIC
    OMX_TI_3ASKIP_IPIPE_GIC_LSC_GAINTYPE eLscGain;
    //!< Slection of threshold vaue in GIC filter
    OMX_TI_3ASKIP_IPIPE_GIC_SELTYPE eSel;
    //!< Selecting the index in GIC
    OMX_TI_3ASKIP_IPIPE_GIC_INDEXTYPE eTyp;
    OMX_U8                            nGicGain;
    OMX_U8                            nGicNfGain;
    OMX_U16                           nGicThr;
    OMX_U16                           nGicSlope;
} OMX_TI_3ASKIP_IPIPE_GIC_CFGTYPE;

typedef struct {
    /*offseet after R,GR,GB,B*/
    OMX_U16 pOffset[4];
    /*gain for R gr gb B*/
    OMX_U16 pGain[4];
} OMX_TI_3ASKIP_IPIPE_WB_CFGTYPE;

typedef enum {
    OMX_IPIPE_CFA_MODE_2DIR    = 0,
    OMX_IPIPE_CFA_MODE_2DIR_DA = 1,
    OMX_IPIPE_CFA_MODE_DAA     = 2,
    OMX_IPIPE_CFA_MODE_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_CFA_MODETYPE;

typedef struct {
    OMX_U16 nHpfThr;
    OMX_U16 nHpfSlope;
    OMX_U16 nMixThr;
    OMX_U16 nMixSlope;
    OMX_U16 nDirThr;
    OMX_U16 nDirSlope;
    OMX_U16 nDirNdwt;
} OMX_TI_3ASKIP_IPIPE_CFA_DIRTYPE;

typedef struct {
    OMX_U8  nMonoHueFra;
    OMX_U8  nMonoEdgThr;
    OMX_U16 nMonoThrMin;
    OMX_U16 nMonoThrSlope;
    OMX_U16 nMonoSlpMin;
    OMX_U16 nMonoSlpSlp;
    OMX_U16 nMonoLpwt;
} OMX_TI_3ASKIP_IPIPE_CFA_DAATYPE;

typedef struct {
    OMX_U8                           nEnable;
    OMX_TI_3ASKIP_IPIPE_CFA_MODETYPE eMode;
    OMX_TI_3ASKIP_IPIPE_CFA_DIRTYPE  tDir;
    OMX_TI_3ASKIP_IPIPE_CFA_DAATYPE  tDaa;
} OMX_TI_3ASKIP_IPIPE_CFA_CFGTYPE;

typedef enum {
    OMX_IPIPE_GAMMA_TBL_64  = 0,
    OMX_IPIPE_GAMMA_TBL_128 = 1,
    OMX_IPIPE_GAMMA_TBL_256 = 2,
    OMX_IPIPE_GAMMA_TBL_512 = 3,
    OMX_IPIPE_GAMMA_TBL_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_GAMMA_TABLE_SIZETYPE;

typedef enum {
    OMX_IPIPE_GAMMA_BYPASS_ENABLE  = 1,
    OMX_IPIPE_GAMMA_BYPASS_DISABLE = 0,
    OMX_IPIPE_GAMMA_BYPASS_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_GAMMA_BYPASSTYPE;

typedef struct {
    OMX_U8                                   nEnable;
    OMX_TI_3ASKIP_IPIPE_GAMMA_TABLE_SIZETYPE eGammaTblSize;
    //!< May not be needed, since table is always in RAM
    OMX_U8 nTbl;
    //!< o not bypassed
    OMX_TI_3ASKIP_IPIPE_GAMMA_BYPASSTYPE eBypassB;
    OMX_TI_3ASKIP_IPIPE_GAMMA_BYPASSTYPE eBypassG;
    OMX_TI_3ASKIP_IPIPE_GAMMA_BYPASSTYPE eBypassR;
    /*poOMX_Ser to red gamma table      Red gamma table -   (U8Q0)
      *                                 Blue gamma table -  (U8Q0)
      *                                 Green gamma table - (U8Q0)
      */
    OMX_S8 pRedTable[OMX_ISS_PREV_GAMMA_TABLE];
    OMX_S8 pBlueTable[OMX_ISS_PREV_GAMMA_TABLE];
    OMX_S8 pGreenTable[OMX_ISS_PREV_GAMMA_TABLE];
} OMX_TI_3ASKIP_IPIPE_GAMMA_CFGTYPE;

typedef struct {
    /*      [RR] [GR] [BR]
     *      [RG] [GG] [BG]
     *      [RB] [GB] [BB]*/
    /* Blending values(S12Q8 format) */
    /*RR,GR,BR,RG,GG,BG,RB,GB,BB each 11 bits*/
    OMX_U16 pMulOff[OMX_ISS_PREV_RGB2RGB_MATRIX][OMX_ISS_PREV_RGB2RGB_MATRIX];
    /* Blending offset value for R,G,B - (S10Q0) */
    /*R,G,B each 13 bits*/
    OMX_U16 pOft[OMX_ISS_PREV_RGB2RGB_OFFSET];
} OMX_TI_3ASKIP_IPIPE_RGBRGB_CFGTYPE;

typedef struct {
    OMX_U8 nBrightness;
    OMX_U8 nContrast;
    /*      [CSCRY]   [CSCGY]  [CSCBY]
     *      [CSCRCB] [CSCGCB] [CSCBCB]
     *      [CSCRCR] [CSCGCR] [CSCBCR] */
    /* Color space conversion coefficients(S10Q8) */
    /*RY,GY,BY,RCB,GCB,BCB ,RCR,GCR,BCR 12 bits*/
    OMX_S16 pMulVal[OMX_ISS_PREV_RGB2YUV_MATRIX][OMX_ISS_PREV_RGB2YUV_MATRIX];
    /* CSC offset values for Y offset, CB offset
     *  and CR offset respectively (S8Q0) */
    /*Y,CB,CR -11bits*/
    OMX_S16 pOffset[OMX_ISS_PREV_RGB2YUV_OFFSET];
} OMX_TI_3ASKIP_IPIPE_RGBYUV_CFGTYPE;

typedef enum {
    //!< Cr CB unmodified
    OMX_IPIPE_GBCE_METHOD_Y_VALUE  = 0,
    OMX_IPIPE_GBCE_METHOD_GAIN_TBL = 1,
    OMX_IPIPE_GBCE_METHOD_MAX      = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_GBCE_METHODTYPE;

typedef struct {
    //!< Enable-disable for GBCE in ipipe
    OMX_U16 nEnable;
    //!< Selecting the type of GBCE method
    OMX_TI_3ASKIP_IPIPE_GBCE_METHODTYPE nTyp;
    //!< GBCE LookUp Tabale
    OMX_U16 pLookupTable[OMX_ISS_GBCE_TABLE_SIZE];
} OMX_TI_3ASKIP_IPIPE_GBCE_CFGTYPE;

typedef enum {
    OMX_IPIPE_PROC_COMPR_NO,
    OMX_IPIPE_PROC_COMPR_DPCM,
    OMX_IPIPE_PROC_COMPR_ALAW,
    OMX_IPIPE_PROC_COMPR_PACK,
    OMX_IPIPE_PROC_COMPR_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_PROC_COMPRESSIONTYPE;

typedef enum {
    OMX_IPIPE_YUV_PHS_POS_COSITED  = 0,
    OMX_IPIPE_YUV_PHS_POS_CENTERED = 1
} OMX_TI_3ASKIP_IPIPE_YUV_PHASE_POSTYPE;

typedef enum  {
    OMX_IPIPE_VP_DEV_CSIA,
    OMX_IPIPE_VP_DEV_CSIB,
    OMX_IPIPE_VP_DEV_CCP,
    OMX_IPIPE_VP_DEV_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_VP_DEVICETYPE;

//ISS configuration structure which controls the operation of yuv444 to yuv 422
typedef struct {
    OMX_TI_3ASKIP_IPIPE_YUV_PHASE_POSTYPE ePos;
    OMX_U8                                nLpfEn;
} OMX_TI_3ASKIP_IPIPE_YUV444YUV422_CFGTYPE;

typedef enum {
    OMX_IPIPE_HALO_REDUCTION_ENABLE  = 1,
    OMX_IPIPE_HALO_REDUCTION_DISABLE = 0,
    OMX_IPIPE_HALO_REDUCTION_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_EE_HALO_CTRLTYPE;

/* ISS configuration structure Edge enhancement */
typedef struct {
    /** Defect Correction Enable */
    OMX_U16 nEnable;
    //Enable-disable for halo reduction in Edge enhancement
    OMX_TI_3ASKIP_IPIPE_EE_HALO_CTRLTYPE eHaloReduction;
    // 9 coefficients
    OMX_S16 pMulVal[OMX_ISS_COEFF];
    OMX_U8  nSel;
    OMX_U8  nShiftHp;
    OMX_U16 nThreshold;
    OMX_U16 nGain;
    OMX_U16 nHpfLowThr;
    OMX_U8  nHpfHighThr;
    OMX_U8  nHpfGradientGain;
    OMX_U8  nHpfgradientOffset;
    OMX_S16 pEeTable[OMX_ISS_EE_TABLE_SIZE];
} OMX_TI_3ASKIP_IPIPE_EE_CFGTYPE;

// ISS configuration structure for CAR module in ipipe
typedef struct {
    // Enable-disable for CAR module in ipipe
    OMX_U8  nEnable;
    OMX_U8  nTyp;
    OMX_U8  nSw0Thr;
    OMX_U8  nSw1Thr;
    OMX_U8  nHpfType;
    OMX_U8  nHpfShift;
    OMX_U8  nHpfThr;
    OMX_U8  nGn1Gain;
    OMX_U8  nGn1Shift;
    OMX_U16 nGn1Min;
    OMX_U8  nGn2Gain;
    OMX_U8  nGn2Shift;
    OMX_U16 nGn2Min;
} OMX_TI_3ASKIP_IPIPE_CAR_CFGTYPE;

//!< ISS configuration structure for LSC
typedef struct {
    OMX_U16 nVOffset;
    //<! va1
    OMX_S16 nVLinearCoeff;
    //<! va2
    OMX_S16 nVQuadraticCoeff;
    //<! vs1
    OMX_U8 nVLinearShift;
    //<! vs2
    OMX_U8  nVQuadraticShift;
    OMX_U16 nHOffset;
    //<! va1
    OMX_S16 nHLinearCoeff;
    //<! va2
    OMX_S16 nHQuadraticCoeff;
    //<! vs1
    OMX_U8 nHLinearShift;
    //<! vs2
    OMX_U8 nHQuadraticShift;
    //!< Gain value for R
    OMX_U8 nGainR;
    //!< Gain value for GR
    OMX_U8 nGainGR;
    //!< Gain value for GB
    OMX_U8 nGainGB;
    //!< Gain value for B
    OMX_U8 nGainB;
    //!< Offset value for R
    OMX_U8 nOffR;
    //!< Offset value for GR
    OMX_U8 nOffGR;
    //!< Offset value for GB
    OMX_U8 nOffGB;
    //!< Offset value for B
    OMX_U8 nOffB;
    //<! LSC_SHIFT
    OMX_U8 nShift;
    //<! LSC_MAX
    OMX_U16 nMax;
} OMX_TI_3ASKIP_IPIPE_LSC_CFGTYPE;

typedef struct {
    OMX_U16 nVPos;
    OMX_U16 nVSize;
    OMX_U16 nHPos;
    OMX_U16 nHSize;
} OMX_TI_3ASKIP_IPIPE_HIST_DIMTYPE;

//!< ISS configuration structure for Histogram in ipipe
typedef struct {
    //!< Enable-disable for Histogram in ipipe
    OMX_U8 nEnable;
    OMX_U8 nOst;
    OMX_U8 nSel;
    OMX_U8 nType;
    OMX_U8 nBins;
    OMX_U8 nShift;
    //!< Bits [3:0], 0 is disable
    OMX_U8 nCol;
    //!< [3:0], 0 is disable
    OMX_U8 nRegions;
    //!< Pointer to array of 4 structs
    OMX_TI_3ASKIP_IPIPE_HIST_DIMTYPE pHistDim[OMX_ISS_HIST_DIMS_COUNT];
    OMX_U8                           nClearTable;
    OMX_U8                           nTableSel;
    //!< r,gr,gb,b
    OMX_U8 pGainTbl[OMX_ISS_HIST_GAIN_TBL];
} OMX_TI_3ASKIP_IPIPE_HIST_CFGTYPE;

typedef enum {
    OMX_BOXCAR_DISABLED     = 0,
    OMX_BOXCAR_ENABLED      = 1,
    OMX_BOXCAR_ENB_DSB_MAX  = 0x7FFFFFFF
} OMX_TI_3ASKIP_BOXCAR_ENABLETYPE;

typedef enum {
    OMX_BOXCAR_FREE_RUN     = 0,
    OMX_BOXCAR_ONE_SHOT     = 1,
    OMX_BOXCAR_FREE_ONE_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_BOXCAR_MODETYPE;

typedef enum {
    OMX_BOXCAR_8x8      = 0,
    OMX_BOXCAR_16x16    = 1,
    OMX_BOXCAR_SIZE_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_BOXCAR_SIZETYPE;

typedef struct {
    OMX_U8 nEnable;
    //!< One shot or free run
    OMX_U8 nOst;
    OMX_U8 nBoxSize;
    OMX_U8 nShift;

    OMX_U32 pAddr[OMX_IPIPE_BOXCAR_MAX_BUFF_SIZE];
} OMX_TI_3ASKIP_IPIPE_BOXCAR_CFGTYPE;

typedef struct {
    OMX_U8 nVectors;
    OMX_U8 nShift;
    //!< Vertical position
    OMX_U16 nVPos;
    //!< Horizontal position
    OMX_U16 nHPos;
    //!< Vertical number
    OMX_U16 nVNum;
    //!< Horizontal number
    OMX_U16 nHNum;
    //!< Horizontal skip
    OMX_U8 nVSkip;
    //!< Vertical skip
    OMX_U8 nHSkip;
} OMX_TI_3ASKIP_IPIPE_BSCPOS_PARAMSTYPE;

typedef struct {
    //!< Enable-disable for BSC in ipipe
    OMX_U8 nEnable;
    //!< BSC mode in ipipe
    OMX_U8 nMode;
    //!< BSC Color Sample
    OMX_U8 nColSample;
    //!< BSC Row Sample
    OMX_U8 nRowSample;
    //!< Y or CB or CR
    OMX_U8 nElement;
    //!< Color Position parameters
    OMX_TI_3ASKIP_IPIPE_BSCPOS_PARAMSTYPE nColPos;
    //!< Row Position parameters
    OMX_TI_3ASKIP_IPIPE_BSCPOS_PARAMSTYPE nRowPos;
} OMX_TI_3ASKIP_IPIPE_BSC_CFGTYPE;

//!< Enable-disable enum for DFS in ipipeif
typedef enum {
    OMX_IPIPEIF_FEATURE_ENABLE  = 1,
    OMX_IPIPEIF_FEATURE_DISABLE = 0,
    OMX_IPIPEIF_FEATURE_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPEIF_FEATURE_SELECTTYPE;

typedef struct {
    //!< Enable-disable for DFS in ipipeif
    OMX_TI_3ASKIP_IPIPEIF_FEATURE_SELECTTYPE eDfsGainEn;
    //!< Valid only if eDfsGainEn = OMX_IPIPEIF_FEATURE_ENABLE
    OMX_U16 nDfsGainVal;
    //!< Valid only if eDfsGainEn = OMX_IPIPEIF_FEATURE_ENABLE
    OMX_U16 nDfsGainThr;
    //!< Valid only if eDfsGainEn = OMX_IPIPEIF_FEATURE_ENABLE
    OMX_U16 nOclip;
    //!< Set to 0 if Sensor Parallel interface data is
    //!< to be subtracted by DRK frm in SDRAM
    OMX_U8 nDfsDir;
} OMX_TI_3ASKIP_IPIPEIF_DFS_CFGTYPE;

//!< ISS struct to control defect pixel corrction in ipipeif
typedef struct {
    //!< Enable-disable for DPC in ipipeif
    OMX_TI_3ASKIP_IPIPEIF_FEATURE_SELECTTYPE eDpcEn;
    OMX_U16                                  eDpcThr;
} OMX_TI_3ASKIP_IPIPEIF_DPC_CFGTYPE;

typedef enum {
    OMX_IPIPEIF_DPCM_PREDICTION_SIMPLE   = 0,
    OMX_IPIPEIF_DPCM_PREDICTION_ADVANCED = 1,
    OMX_IPIPEIF_DPCM_PREDICTION_MAX      = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPEIF_DPCM_PRED_TYPTYPE;

typedef enum {
    OMX_IPIPEIF_DPCM_BIT_SIZE_8_10 = 0,
    OMX_IPIPEIF_DPCM_BIT_SIZE_8_12 = 1,
    OMX_IPIPEIF_DPCM_BIT_SIZE_MAX = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPEIF_DPCM_BIT_SIZETYPE;

typedef struct {
    //!< Enable-disable for DPCM in ipipeif
    OMX_TI_3ASKIP_IPIPEIF_FEATURE_SELECTTYPE nDpcmEn;
    //!< Valid only if DPCM is enabled; dpcm_en=1
    OMX_TI_3ASKIP_IPIPEIF_DPCM_PRED_TYPTYPE nDpcmPredictor;
    //!< Valid only if DPCM is enabled; dpcm_en=1
    OMX_TI_3ASKIP_IPIPEIF_DPCM_BIT_SIZETYPE nDpcmBitSize;
} OMX_TI_3ASKIP_IPIPEIF_DPCM_CFGTYPE;

typedef enum {
    OMX_RSZ_IP_IPIPE   = 0,
    OMX_RSZ_IP_IPIPEIF = 1,
    OMX_RSZ_IP_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_RSZ_IP_PORT_SELTYPE;

typedef enum {
    OMX_MEM_INPUT_IPIPEIF,
    OMX_MEM_INPUT_CCP,
    OMX_MEM_INPUT_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_MEM_PROC_INPUT_SELECTTYPE;

//!< ISS low pass filter params for Horizontal resizing
typedef struct {
    OMX_U8 nCIntensity;
    OMX_U8 nYIntensity;
} OMX_TI_3ASKIP_RSZ_LPF_CFGTYPE;

//!< Enable-Disable H3A Features
typedef enum {
    OMX_H3A_FEATURE_DISABLE = 0,
    OMX_H3A_FEATURE_ENABLE  = 1,
    OMX_H3A_FEATURE_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE;

//!< H3A Paxel Configuration
typedef struct {
    //!< AEWINSTART WINSV  AFPAXSTART PAXSV
    OMX_U16 nVPos;
    //!< AEWWIN1 WINW      AFPAX1 PAXH
    OMX_U8 nVSize;
    //!< AEWINSTART WINSH  AFPAXSTART PAXSH
    OMX_U16 nHPos;
    //!< AEWWIN1 WINH      AFPAX1 PAXW
    OMX_U8 nHSize;
    //!< AEWWIN1 WINVC     AFPAX2 PAXVC
    OMX_U8 nVCount;
    //!< AEWSUBWIN AEWINCV AFPAX2 AFINCV
    OMX_U8 nVIncr;
    //!< AEWWIN1 WINHC     AFPAX2 PAXHC
    OMX_U8 nHCount;
    //!< AEWSUBWIN AEWINCH AFPAX2 AFINCH
    OMX_U8 nHIncr;
} OMX_TI_3ASKIP_H3A_PAXEL_CFGTYPE;

typedef struct {
    //!< AEWINBLK WINSV single row of black line vpos
    OMX_U16 nVPos;
    //!< AEWINBLK WINH  win height
    OMX_U16 nHPos;
} OMX_TI_3ASKIP_H3A_AEWB_BLKDIMSTYPE;

typedef enum {
    OMX_H3A_AEWB_OP_FMT_SUM_OF_SQR = 0,
    OMX_H3A_AEWB_OP_FMT_MINMAX     = 1,
    OMX_H3A_AEWB_OP_FMT_SUM_ONLY   = 2,
    OMX_H3A_AEWB_OP_FMT_MAX        = 0x7FFFFFFF
} OMX_TI_3ASKIP_H3A_AEWB_OP_FMTTYPE;


typedef struct {
    //!< Enable-disable the Aewb engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eAewbEnable;
    //!< Paxel config
    OMX_TI_3ASKIP_H3A_PAXEL_CFGTYPE tAewbPaxelWin;
    //!< AEWB Block Dimensions
    OMX_TI_3ASKIP_H3A_AEWB_BLKDIMSTYPE tBlkWinDims;
    //!< AEWCFG AEFMT
    OMX_TI_3ASKIP_H3A_AEWB_OP_FMTTYPE eAeOpFmt;
    //!< AEWCFG SUMFST
    OMX_U8 nShiftValue;
    //PCR AVE2LMT
    OMX_U16 nSaturationLimit;
    //AEWBUFST AEWBUFST, 64 bit aligned address
    OMX_U32 pAewbOpAddr[OMX_AEWB_H3A_MAX_BUFF_SIZE];
} OMX_TI_3ASKIP_H3A_AEWB_PARAMTYPE;

typedef enum {
    OMX_H3A_AF_RGBPOS_GR_GB_BAYER  = 0,
    OMX_H3A_AF_RGBPOS_RG_GB_BAYER  = 1,
    OMX_H3A_AF_RGBPOS_GR_BG_BAYER  = 2,
    OMX_H3A_AF_RGBPOS_RG_BG_BAYER  = 3,
    OMX_H3A_AF_RGBPOS_GG_RB_CUSTOM = 4,
    OMX_H3A_AF_RGBPOS_RB_GG_CUSTOM = 5,
    OMX_H3A_AF_RGBPOS_MAX          = 0x7FFFFFFF
} OMX_TI_3ASKIP_H3A_RGB_POSTYPE;

typedef struct {
    //AFCOEF010 COEFF1
    OMX_U16 pIirCoef[11];
    OMX_U16 nHfvThres;
} OMX_TI_3ASKIP_H3A_AF_IIRPARAMTYPE;

typedef struct {
    OMX_U8 pFirCoef[5];
    OMX_U8 nVfvThres;
} OMX_TI_3ASKIP_H3A_AF_FIRPARAMTYPE;

typedef struct {
    //!< Enable-Disable the AF engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eAfEnable;
    //!< Valid only if vertical focus is enabled
    OMX_TI_3ASKIP_H3A_RGB_POSTYPE eRgbPos;

    //!< Enable-Disable the PeakMode engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE ePeakModeEn;
    //!< Enable-Disable the VerticalFocus engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eVerticalFocusEn;

    //!< AFIIRSH IIRSH
    OMX_U16 nIirStartPos;

    //!< Paxel config
    OMX_TI_3ASKIP_H3A_PAXEL_CFGTYPE tAfPaxelWin;

    OMX_TI_3ASKIP_H3A_AF_IIRPARAMTYPE tIir1;
    OMX_TI_3ASKIP_H3A_AF_IIRPARAMTYPE tIir2;

    OMX_TI_3ASKIP_H3A_AF_FIRPARAMTYPE tFir1;
    OMX_TI_3ASKIP_H3A_AF_FIRPARAMTYPE tFir2;
    //!< AEWBUFST AEWBUFST, 64 bit aligned address
    OMX_U32 pAfOpAddr[OMX_AF_H3A_MAX_BUFF_SIZE];
} OMX_TI_3ASKIP_H3A_AF_PARAMTYPE;

typedef struct {
    //!< Enable-disable for H3A AF Median engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eAfMedianEn;
    //!< Enable-disable for H3A AEWB Median engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eAewbMedianEn;
    //!< Valid only if eAfMedianEn is set to OMX_H3A_FEATURE_ENABLE
    OMX_U8 nMedianFilterThreshold;
    //!< Enable-disable for AFAlaw engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eAfAlawEn;
    //!< Enable-disable for AEWBAlaw engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eAewbAlawEn;
    //!< Enable-disable for IPIPEIFAve filter engine
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eIpipeifAveFiltEn;
    //!< Enable-disable for H3A decimation
    OMX_TI_3ASKIP_H3A_FEATURE_ENABLETYPE eH3aDecimEnable;
    //!< Reserved
    OMX_U32 nReserved;
} OMX_TI_3ASKIP_H3A_COMMON_CFGTYPE;

typedef struct {
    //Enable-disable for 3Dcc
    OMX_U8 enable;
    //3D LUT
    OMX_U32 pInBiffData[OMX_ISS_3D_LUT_SIZE];
    //OMX_Ipipe3DLutDataT   eDataType;
    //OMX_U32               tblAddress;
} OMX_TI_3ASKIP_IPIPE_3DCC_CFGTYPE;

typedef enum {
    OMX_IPIPE_DPC_LUT_REPLACE_BLACK = 0,
    OMX_IPIPE_DPC_LUT_REPLACE_WHITE = 1,
    OMX_IPIPE_DPC_LUT_REPLACE_MAX   = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_DPC_LUT_REPLACEMENTTYPE;

typedef enum {
    OMX_IPIPE_DPC_LUT_TBL_SIZE_1024 = 0,
    OMX_IPIPE_DPC_LUT_TBL_SIZE_INF  = 1,
    OMX_IPIPE_DPC_LUT_TBL_SIZE_MAX  = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_DPC_LUT_TBL_SIZETYPE;

typedef struct {
    //Enable-disable for DPC Lut
    OMX_U16                                     nEnable;
    OMX_TI_3ASKIP_IPIPE_DPC_LUT_TBL_SIZETYPE    eTableType;
    OMX_TI_3ASKIP_IPIPE_DPC_LUT_REPLACEMENTTYPE eReplaceType;
    OMX_U16                                     pLutValidAddr[256];
    OMX_U16                                     nLutSize;
    OMX_U32                                     pLutTable0Addr[256];
    OMX_U32                                     pLutTable1Addr[256];
} OMX_TI_3ASKIP_IPIPE_DPCLUT_CFGTYPE;

typedef enum {
    OMX_IPIPE_DPC_OTF_ALG_MINMAX2 = 0,
    OMX_IPIPE_DPC_OTF_ALG_MINMAX3 = 1,
    OMX_IPIPE_DPC_OTF_ALG_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_DPC_ALGOTYPE;

typedef enum {
    OMX_IPIPE_DPC_OTF_MAX1_MIN1 = 0,
    OMX_IPIPE_DPC_OTF_MAX2_MIN2 = 1,
    OMX_IPIPE_DPC_OTF_MAX       = 0x7FFFFFFF
} OMX_TI_3ASKIP_IPIPE_DPC_OTFTYPE;

typedef struct {
    OMX_U16 thr_cor_r;
    OMX_U16 thr_cor_gr;
    OMX_U16 thr_cor_gb;
    OMX_U16 thr_cor_b;

    OMX_U16 thr_det_r;
    OMX_U16 thr_det_gr;
    OMX_U16 thr_det_gb;
    OMX_U16 thr_det_b;
} OMX_TI_3ASKIP_IPIPE_DPCOTF_DPC2TYPE;

typedef struct {
    OMX_U8  nDThr;
    OMX_U8  nDSlp;
    OMX_U16 nDMin;
    OMX_U16 nDMax;
} OMX_TI_3ASKIP_IPIPE_DPCOFT_FILTERTYPE;

typedef struct {
    OMX_U8                                eShift;
    OMX_TI_3ASKIP_IPIPE_DPCOFT_FILTERTYPE eOtfCorr;
    OMX_TI_3ASKIP_IPIPE_DPCOFT_FILTERTYPE eOtfDett;
} OMX_TI_3ASKIP_IPIPE_DPCOTF_DPC3TYPE;

/* Union which helps selec either dpc2-dpc 3 params */
typedef union {
    OMX_TI_3ASKIP_IPIPE_DPCOTF_DPC2TYPE tDpc2Params;
    OMX_TI_3ASKIP_IPIPE_DPCOTF_DPC3TYPE tDpc3Params;
} OMX_TI_3ASKIP_IPIPE_DPCOTF_FILTER_PARAMSTYPE;

typedef struct {
    //!< Enable-disable for DPC
    OMX_U8 nEnable;
    //!< ISS dpc otf type
    OMX_TI_3ASKIP_IPIPE_DPC_OTFTYPE eType;
    //!< ISS dpc otf definitions
    OMX_TI_3ASKIP_IPIPE_DPC_ALGOTYPE eAlgo;
    //!< Union which helps selec either dpc2-dpc 3 params
    OMX_TI_3ASKIP_IPIPE_DPCOTF_FILTER_PARAMSTYPE tDpcData;
} OMX_TI_3ASKIP_IPIPE_DPCOTF_CFGTYPE;

typedef struct {
    OMX_U16 thr;
    OMX_U16 gain;
    OMX_U16 shift;
    OMX_U16 min;
} OMX_TI_3ASKIP_IPIPE_CHROMA_PARAMSTYPE;

typedef struct {
    //!< Enable-disable for CGC in ipipe
    OMX_U16                               enable;
    OMX_TI_3ASKIP_IPIPE_CHROMA_PARAMSTYPE y_chroma_low;
    OMX_TI_3ASKIP_IPIPE_CHROMA_PARAMSTYPE y_chroma_high;
    OMX_TI_3ASKIP_IPIPE_CHROMA_PARAMSTYPE c_chroma;
} OMX_TI_3ASKIP_IPIPE_CGS_CFGTYPE;

typedef enum {
    OMX_TRANSFER_ONLINE  = 0,
    OMX_TRANSFER_OFFLINE = 1,
    OMX_TRANSFER_MAX     = 0x7FFFFFFF
} OMX_TI_3ASKIP_CAM_TRANSFERTYPE;

typedef struct {
    OMX_U32 exp;
    OMX_U32 a_gain;
    OMX_U8  mask;
    OMX_U32 nAgainErr;
    OMX_U32 nDigitalISPGain;
} OMX_TI_3ASKIP_CAM_CONTROL_EXPGAINTYPE;

typedef enum {
    /* IDs for L sensor and mono usecases */
    OMX_TI_3aSkipIndexLLeftStart = 0,
    OMX_TI_3aSkipIndexL_ColorPattern = OMX_TI_3aSkipIndexLLeftStart,    /**< 0x00000000 reference: OMX_TI_3ASKIP_IPIPE_BAYER_PATTERNTYPE */
    OMX_TI_3aSkipIndexL_MsbPos,             /**< 0x00000001 reference: OMX_TI_3ASKIP_IPIPE_BAYER_MSB_POSTYPE */
    OMX_TI_3aSkipIndexL_VpDevice,           /**< 0x00000002 reference: OMX_TI_3ASKIP_IPIPE_VP_DEVICETYPE */

    OMX_TI_3aSkipIndexL_Lsc2D,              /**< 0x00000003 reference: OMX_TI_3ASKIP_ISIF_2DLSC_CFGTYPE */
    OMX_TI_3aSkipIndexL_Clamp,              /**< 0x00000004 reference: OMX_TI_3ASKIP_ISIF_CLAMP_CFGTYPE */
    OMX_TI_3aSkipIndexL_Flash,              /**< 0x00000005 reference: OMX_TI_3ASKIP_ISIF_FLASH_CFGTYPE */
    OMX_TI_3aSkipIndexL_GainOffset,         /**< 0x00000006 reference: OMX_TI_3ASKIP_ISIF_GAINOFFSET_CFGTYPE */
    OMX_TI_3aSkipIndexL_Vlcd,               /**< 0x00000007 reference: OMX_TI_3ASKIP_ISIF_VLDC_CFGTYPE */

    OMX_TI_3aSkipIndexL_Nf1,                /**< 0x00000008 reference: OMX_TI_3ASKIP_IPIPE_NOISE_FILTER_CFGTYPE */
    OMX_TI_3aSkipIndexL_Nf2,                /**< 0x00000009 reference: OMX_TI_3ASKIP_IPIPE_NOISE_FILTER_CFGTYPE */
    OMX_TI_3aSkipIndexL_GIC,                /**< 0x0000000A reference: OMX_TI_3ASKIP_IPIPE_GIC_CFGTYPE */
    OMX_TI_3aSkipIndexL_WB,                 /**< 0x0000000B reference: OMX_TI_3ASKIP_IPIPE_WB_CFGTYPE */
    OMX_TI_3aSkipIndexL_CFA,                /**< 0x0000000C reference: OMX_TI_3ASKIP_IPIPE_CFA_CFGTYPE */
    OMX_TI_3aSkipIndexL_Gamma,              /**< 0x0000000D reference: OMX_TI_3ASKIP_IPIPE_GAMMA_CFGTYPE */
    OMX_TI_3aSkipIndexL_Rgb2Rgb1,           /**< 0x0000000E reference: OMX_TI_3ASKIP_IPIPE_RGBRGB_CFGTYPE */
    OMX_TI_3aSkipIndexL_Rgb2Rgb2,           /**< 0x0000000F reference: OMX_TI_3ASKIP_IPIPE_RGBRGB_CFGTYPE */
    OMX_TI_3aSkipIndexL_Rgb2Yuv,            /**< 0x00000010 reference: OMX_TI_3ASKIP_IPIPE_RGBYUV_CFGTYPE */
    OMX_TI_3aSkipIndexL_GBCE,               /**< 0x00000011 reference: OMX_TI_3ASKIP_IPIPE_GBCE_CFGTYPE */
    OMX_TI_3aSkipIndexL_Yuv2Yuv,            /**< 0x00000012 reference: OMX_TI_3ASKIP_IPIPE_YUV444YUV422_CFGTYPE */
    OMX_TI_3aSkipIndexL_Ee,                 /**< 0x00000013 reference: OMX_TI_3ASKIP_IPIPE_EE_CFGTYPE */
    OMX_TI_3aSkipIndexL_Car,                /**< 0x00000014 reference: OMX_TI_3ASKIP_IPIPE_CAR_CFGTYPE */
    OMX_TI_3aSkipIndexL_Lsc,                /**< 0x00000015 reference: OMX_TI_3ASKIP_IPIPE_LSC_CFGTYPE */
    OMX_TI_3aSkipIndexL_Histogram,          /**< 0x00000016 reference: OMX_TI_3ASKIP_IPIPE_HIST_CFGTYPE */
    OMX_TI_3aSkipIndexL_Boxcar,             /**< 0x00000017 reference: OMX_TI_3ASKIP_IPIPE_BOXCAR_CFGTYPE */
    OMX_TI_3aSkipIndexL_Bsc,                /**< 0x00000018 reference: OMX_TI_3ASKIP_IPIPE_BSC_CFGTYPE */
    OMX_TI_3aSkipIndexL_3Dcc,               /**< 0x00000019 reference: OMX_TI_3ASKIP_IPIPE_3DCC_CFGTYPE */
    OMX_TI_3aSkipIndexL_DpcOtf,             /**< 0x0000001A reference: OMX_TI_3ASKIP_IPIPE_DPCOTF_CFGTYPE */
    OMX_TI_3aSkipIndexL_DpcLut,             /**< 0x0000001B reference: OMX_TI_3ASKIP_IPIPE_DPCLUT_CFGTYPE */
    OMX_TI_3aSkipIndexL_Cgs,                /**< 0x0000001C reference: OMX_TI_3ASKIP_IPIPE_CGS_CFGTYPE */

    OMX_TI_3aSkipIndexL_Dfs,                /**< 0x0000001D reference: OMX_TI_3ASKIP_IPIPEIF_DFS_CFGTYPE */
    OMX_TI_3aSkipIndexL_Dpc1,               /**< 0x0000001E reference: OMX_TI_3ASKIP_IPIPEIF_DPC_CFGTYPE */
    OMX_TI_3aSkipIndexL_Dpc2,               /**< 0x0000001F reference: OMX_TI_3ASKIP_IPIPEIF_DPC_CFGTYPE */
    OMX_TI_3aSkipIndexL_Dpcm,               /**< 0x00000020 reference: OMX_TI_3ASKIP_IPIPEIF_DPCM_CFGTYPE */

    OMX_TI_3aSkipIndexL_HLpf,               /**< 0x00000021 reference: OMX_TI_3ASKIP_RSZ_LPF_CFGTYPE */
    OMX_TI_3aSkipIndexL_VLpf,               /**< 0x00000022 reference: OMX_TI_3ASKIP_RSZ_LPF_CFGTYPE */

    OMX_TI_3aSkipIndexL_H3aAewbParams,      /**< 0x00000023 reference: OMX_TI_3ASKIP_H3A_AEWB_PARAMTYPE */
    OMX_TI_3aSkipIndexL_H3aAfParams,        /**< 0x00000024 reference: OMX_TI_3ASKIP_H3A_AF_PARAMTYPE */
    OMX_TI_3aSkipIndexL_H3aCommonParams,    /**< 0x00000025 reference: OMX_TI_3ASKIP_H3A_COMMON_CFGTYPE */
    OMX_TI_3aSkipIndexL_CamControlExpGain,  /**< 0x00000026 reference: OMX_TI_3ASKIP_CAM_CONTROL_EXPGAINTYPE */
    OMX_TI_3aSkipIndexLLeftMax,             /**< 0x00000027 */

    /* IDs for R sensor */
    OMX_TI_3aSkipIndexRightStart = 0x01000000,
    OMX_TI_3aSkipIndexR_ColorPattern = OMX_TI_3aSkipIndexRightStart,    /**< 0x01000000 reference: OMX_TI_3ASKIP_IPIPE_BAYER_PATTERNTYPE */
    OMX_TI_3aSkipIndexR_MsbPos,             /**< 0x01000001 reference: OMX_TI_3ASKIP_IPIPE_BAYER_MSB_POSTYPE */
    OMX_TI_3aSkipIndexR_VpDevice,           /**< 0x01000002 reference: OMX_TI_3ASKIP_IPIPE_VP_DEVICETYPE */

    OMX_TI_3aSkipIndexR_Lsc2D,              /**< 0x01000003 reference: OMX_TI_3ASKIP_ISIF_2DLSC_CFGTYPE */
    OMX_TI_3aSkipIndexR_Clamp,              /**< 0x01000004 reference: OMX_TI_3ASKIP_ISIF_CLAMP_CFGTYPE */
    OMX_TI_3aSkipIndexR_Flash,              /**< 0x01000005 reference: OMX_TI_3ASKIP_ISIF_FLASH_CFGTYPE */
    OMX_TI_3aSkipIndexR_GainOffset,         /**< 0x01000006 reference: OMX_TI_3ASKIP_ISIF_GAINOFFSET_CFGTYPE */
    OMX_TI_3aSkipIndexR_Vlcd,               /**< 0x01000007 reference: OMX_TI_3ASKIP_ISIF_VLDC_CFGTYPE */

    OMX_TI_3aSkipIndexR_Nf1,                /**< 0x01000008 reference: OMX_TI_3ASKIP_IPIPE_NOISE_FILTER_CFGTYPE */
    OMX_TI_3aSkipIndexR_Nf2,                /**< 0x01000009 reference: OMX_TI_3ASKIP_IPIPE_NOISE_FILTER_CFGTYPE */
    OMX_TI_3aSkipIndexR_GIC,                /**< 0x0100000A reference: OMX_TI_3ASKIP_IPIPE_GIC_CFGTYPE */
    OMX_TI_3aSkipIndexR_WB,                 /**< 0x0100000B reference: OMX_TI_3ASKIP_IPIPE_WB_CFGTYPE */
    OMX_TI_3aSkipIndexR_CFA,                /**< 0x0100000C reference: OMX_TI_3ASKIP_IPIPE_CFA_CFGTYPE */
    OMX_TI_3aSkipIndexR_Gamma,              /**< 0x0100000D reference: OMX_TI_3ASKIP_IPIPE_GAMMA_CFGTYPE */
    OMX_TI_3aSkipIndexR_Rgb2Rgb1,           /**< 0x0100000E reference: OMX_TI_3ASKIP_IPIPE_RGBRGB_CFGTYPE */
    OMX_TI_3aSkipIndexR_Rgb2Rgb2,           /**< 0x0100000F reference: OMX_TI_3ASKIP_IPIPE_RGBRGB_CFGTYPE */
    OMX_TI_3aSkipIndexR_Rgb2Yuv,            /**< 0x01000010 reference: OMX_TI_3ASKIP_IPIPE_RGBYUV_CFGTYPE */
    OMX_TI_3aSkipIndexR_GBCE,               /**< 0x01000011 reference: OMX_TI_3ASKIP_IPIPE_GBCE_CFGTYPE */
    OMX_TI_3aSkipIndexR_Yuv2Yuv,            /**< 0x01000012 reference: OMX_TI_3ASKIP_IPIPE_YUV444YUV422_CFGTYPE */
    OMX_TI_3aSkipIndexR_Ee,                 /**< 0x01000013 reference: OMX_TI_3ASKIP_IPIPE_EE_CFGTYPE */
    OMX_TI_3aSkipIndexR_Car,                /**< 0x01000014 reference: OMX_TI_3ASKIP_IPIPE_CAR_CFGTYPE */
    OMX_TI_3aSkipIndexR_Lsc,                /**< 0x01000015 reference: OMX_TI_3ASKIP_IPIPE_LSC_CFGTYPE */
    OMX_TI_3aSkipIndexR_Histogram,          /**< 0x01000016 reference: OMX_TI_3ASKIP_IPIPE_HIST_CFGTYPE */
    OMX_TI_3aSkipIndexR_Boxcar,             /**< 0x01000017 reference: OMX_TI_3ASKIP_IPIPE_BOXCAR_CFGTYPE */
    OMX_TI_3aSkipIndexR_Bsc,                /**< 0x01000018 reference: OMX_TI_3ASKIP_IPIPE_BSC_CFGTYPE */
    OMX_TI_3aSkipIndexR_3Dcc,               /**< 0x01000019 reference: OMX_TI_3ASKIP_IPIPE_3DCC_CFGTYPE */
    OMX_TI_3aSkipIndexR_DpcOtf,             /**< 0x0100001A reference: OMX_TI_3ASKIP_IPIPE_DPCOTF_CFGTYPE */
    OMX_TI_3aSkipIndexR_DpcLut,             /**< 0x0100001B reference: OMX_TI_3ASKIP_IPIPE_DPCLUT_CFGTYPE */
    OMX_TI_3aSkipIndexR_Cgs,                /**< 0x0100001C reference: OMX_TI_3ASKIP_IPIPE_CGS_CFGTYPE */

    OMX_TI_3aSkipIndexR_Dfs,                /**< 0x0100001D reference: OMX_TI_3ASKIP_IPIPEIF_DFS_CFGTYPE */
    OMX_TI_3aSkipIndexR_Dpc1,               /**< 0x0100001E reference: OMX_TI_3ASKIP_IPIPEIF_DPC_CFGTYPE */
    OMX_TI_3aSkipIndexR_Dpc2,               /**< 0x0100001F reference: OMX_TI_3ASKIP_IPIPEIF_DPC_CFGTYPE */
    OMX_TI_3aSkipIndexR_Dpcm,               /**< 0x01000020 reference: OMX_TI_3ASKIP_IPIPEIF_DPCM_CFGTYPE */

    OMX_TI_3aSkipIndexR_HLpf,               /**< 0x01000021 reference: OMX_TI_3ASKIP_RSZ_LPF_CFGTYPE */
    OMX_TI_3aSkipIndexR_VLpf,               /**< 0x01000022 reference: OMX_TI_3ASKIP_RSZ_LPF_CFGTYPE */

    OMX_TI_3aSkipIndexR_H3aAewbParams,      /**< 0x01000023 reference: OMX_TI_3ASKIP_H3A_AEWB_PARAMTYPE */
    OMX_TI_3aSkipIndexR_H3aAfParams,        /**< 0x01000024 reference: OMX_TI_3ASKIP_H3A_AF_PARAMTYPE */
    OMX_TI_3aSkipIndexR_H3aCommonParams,    /**< 0x01000025 reference: OMX_TI_3ASKIP_H3A_COMMON_CFGTYPE */
    OMX_TI_3aSkipIndexR_CamControlExpGain,  /**< 0x01000026 reference: OMX_TI_3ASKIP_CAM_CONTROL_EXPGAINTYPE */
    OMX_TI_Index3aSkipRightMax,             /**< 0x01000027 */
    OMX_TI_Index3aSkip_MAX = 0x7FFFFFFF

} OMX_TI_3ASKIP_TI_3ASKIPINDEXTYPE;

/*--------function prototypes ---------------------------------*/

/****************************************************************
 * PRIVATE DECLARATIONS Defined here, used only here
 ****************************************************************/
/*--------data declarations -----------------------------------*/

/*--------macros ----------------------------------------------*/

/*--------function prototypes ---------------------------------*/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _OMX_MOD_CORRECTIONS_H */

